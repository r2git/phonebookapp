$(document).ready(function(){

    const $contactForm = $('.add-contact-form');
    const $contactList = $('.contact-list');

    function resetForm(){
        resetFormErrors();        
        $contactForm.find('input[type=text]').val('');
        $contactForm.removeClass('loading');
    }

    function resetFormErrors(){
        $contactForm.find('.field.error').removeClass('error');
        $contactForm.find('.error').remove();
    }

    $('#add-contact-button').click((e) => {
        e.preventDefault();
        resetForm();
        $contactForm.toggleClass('visible');
        setTimeout(() => {
            $contactForm.find('input[type=text]').eq(0).focus();
        }, 300);                
    });

    $contactForm.find('.close-button').click((e) => {
        e.preventDefault();
        $contactForm.toggleClass('visible');
    });

    $contactForm.find('.submit-button').click((e) => {
        
        e.preventDefault();
        $contactForm.addClass('loading');
        
        resetFormErrors();

        const url = $contactForm.find('form').attr('action');
        let formData = new FormData;
        formData.append('submit-contacts-form', true);

        $contactForm.find('input').each((i, input) => {
            const $input = $(input);
            console.log('type', $input.attr('type'));
            if ($input.attr('type') == 'submit'){
                return;
            }
            const value = $input.attr('type') == 'file' ? $input.prop('files')[0] : $input.val();
            formData.append($input.attr('name'), value);
        });

        $.ajax({
            url: url,
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {
                $contactForm.removeClass('loading');

                if (result.success){
                    $('.empty').remove();
                    $contactList.append($(result.html));
                    $contactForm.toggleClass('visible');
                    $contactForm.find('input[type=text]').val('');
                    return;
                }
    
                if (result.errors){
                    Object.keys(result.errors).forEach((fieldName) => {
                        const errorText = result.errors[fieldName];
                        const $input = $contactForm.find(`input[name=${fieldName}]`);
                        $input.parent('.input').after(`<div class="error">${errorText}</div>`);
                        $input.parents('.field').addClass('error');
                    });
                }   
            }
        });

    });

});