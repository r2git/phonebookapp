Приложение "Телефонная книга"

Базовый MVC-фреймворк, простой ORM, СRUD.

Для запуска:
- Apache + mod_rewrite, PHP 7.2, MySQL;
- Дамп базы в database.sql;
- Параметры базы прописать в app/config.php;
- Сделать chmod 777 на папку uploads;
