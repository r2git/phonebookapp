<?php

try {
    require 'app/bootstrap.php';
    \PhoneBookApp\Core\Router::route();
} catch (Exception $e) {
    \PhoneBookApp\Core\Response::exception($e);
}
