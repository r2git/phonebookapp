<?php 

namespace PhoneBookApp\Controllers;

use PhoneBookApp\Core\Controller;
use PhoneBookApp\Core\Response;
use PhoneBookApp\Core\Request;
use PhoneBookApp\Core\Session;
use PhoneBookApp\Models\Contact;

class contacts extends Controller {

    public $isAuthRequired = true;

    public function actionIndex(){ 

        $contactForm = $this->getAddContactForm();
        $user = Session::getUser();
        $contacts = Contact::getAll(['user_id' => $user->id]);

        Response::view('contacts/contacts', [
            'contactForm' => $contactForm,
            'contacts' => $contacts
        ]);

    }

    public function actionAdd(){

        if (!Request::isAjax()){ Response::error404(); }

        $contactForm = $this->getAddContactForm();

        if ($contactForm->isSubmitted()){
            if ($contactForm->isValid()){

                $user = Session::getUser();
                $result = $contactForm->getResult();
                $contact = Contact::createFromArray($result);
                $contact->user_id = $user->id;
                $contact->uploadFiles();                
                $contact->save();

                Response::json([
                    'success' => true,
                    'html' => $contact->render()
                ]);

            }
        }

        Response::json([
            'success' => false,
            'errors' => $contactForm->getErrors()
        ]);

    }

    public function actionDelete($id){
        
        $user = Session::getUser();
        $contact = Contact::getById($id);

        if (!$contact || $contact->user_id != $user->id){
            Response::error404();
        }

        $contact->delete();

        Response::redirect(url('contacts'));

    }

    public function actionView($id){

        $user = Session::getUser();
        $contact = Contact::getById($id);

        if (!$contact || $contact->user_id != $user->id){
            Response::error404();
        }

        Response::view('contacts/view', [
            'contact' => $contact
        ]);

    }

    private function getAddContactForm(){
        $form = Contact::getForm();
        $form->isAjax = true;
        $form->endpointURL = url('contacts', 'add');
        $form->submitButtonText = 'Добавить';
        return $form;
    }

}
