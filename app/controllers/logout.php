<?php 

namespace PhoneBookApp\Controllers;

use PhoneBookApp\Core\Controller;
use PhoneBookApp\Core\Session;
use PhoneBookApp\Core\Response;

class logout extends Controller {

    public function actionIndex(){ 
        Session::logout();
        Response::redirect(url('/'));
    }
    
}
