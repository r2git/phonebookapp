<?php 

namespace PhoneBookApp\Controllers;

use PhoneBookApp\Core\Controller;
use PhoneBookApp\Core\Form;
use PhoneBookApp\Core\Response;
use PhoneBookApp\Core\Session;
use PhoneBookApp\Core\Notification;
use PhoneBookApp\Models\User;
use PhoneBookApp\Core\Exceptions\Validation_Exception;

class signup extends Controller {

    public function actionIndex(){ 

        if (Session::isLogged()){
            Response::redirect(url('contacts'));
        }

        $signupForm = $this->createSignupForm();

        if ($signupForm->isSubmitted()){            
            if ($signupForm->isValid()) {
                try {
                    $result = $signupForm->getResult();
                    if ($result['password'] != $result['password2']){
                        throw new Validation_Exception('Пароли не совпали');
                    }
                    $user = User::createFromArray($result);
                    $user->save();
                    Session::login($user);
                    Session::addNotification(new Notification('success', 'Регистрация прошла успешно!'));                    
                    Response::redirect(url('/'));
                } catch (Validation_Exception $e) {
                    Session::addNotification(new Notification('error', $e->getMessage()));
                }
            } 
            $signupForm->clearFields(['password', 'password2']);
        }

        Response::view('signup', [
            'signupForm' => $signupForm
        ]);

    }

    private function createSignupForm(){

        $form = User::getForm();
        $form->submitButtonText = 'Регистрация';

        $form->addField([
            'name' => 'password2',
            'type' => 'password',
            'title' => 'Повторите пароль',
            'isRequired' => true,
            'options' => [
                'minLength' => 6,
                'maxLength' => 32,
                'skipPasswordStrengthCheck' => true
            ]
        ]);

        return $form;

    }

}
