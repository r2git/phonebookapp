<?php 

namespace PhoneBookApp\Controllers;

use PhoneBookApp\Core\Controller;
use PhoneBookApp\Core\Form;
use PhoneBookApp\Core\Response;
use PhoneBookApp\Core\Session;
use PhoneBookApp\Core\Notification;
use PhoneBookApp\Models\User;

class index extends Controller {

    public function actionIndex(){ 

        if (Session::isLogged()){
            Response::redirect(url('contacts'));
        }

        $loginForm = $this->createLoginForm();

        if ($loginForm->isSubmitted()){            
            if ($loginForm->isValid()) {
                $result = $loginForm->getResult();
                $users = User::getAll([
                    'email' => $result['email'],
                    'password' => User::getPasswordHash($result['password'])
                ]);
                if ($users && count($users) == 1){                                    
                    Session::login($users[0]);
                    Response::redirect(url('contacts'));
                } else {
                    $loginForm->clearFields();
                    Session::addNotification(new Notification('error', 'Почта или пароль указаны неверно'));
                }
            } 
        }

        Response::view('index', [
            'loginForm' => $loginForm
        ]);

    }

    private function createLoginForm(){

        $form = new Form('login-form');
        $form->submitButtonText = 'Войти';        

        $form->addField([
            'name' => 'email',
            'type' => 'email',
            'title' => 'Электронная почта',
            'isRequired' => true,
            'options' => [
                'maxLength' => 255
            ]
        ]);

        $form->addField([
            'name' => 'password',
            'type' => 'password',
            'title' => 'Пароль',
            'isRequired' => true, 
            'options' => [
                'minLength' => 6,
                'maxLength' => 32,
                'skipPasswordStrengthCheck' => true
            ]
        ]);

        return $form;

    }

}
