<?php

namespace PhoneBookApp\Core;

class Notification {

    public $type = 'success';
    public $message = '';

    public function __construct($type, $message){
        $this->type = $type;
        $this->message = $message;
    }

    public function toArray(){
        return [
            'type' => $this->type,
            'message'=> $this->message
        ];
    }

    public static function fromArray($array){
        return new self($array['type'], $array['message']);
    }

}
