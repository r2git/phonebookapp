<?php 

namespace PhoneBookApp\Core;

class Field {

    private $name;
    private $type;
    private $title;
    private $isRequired = false;
    private $isVirtual = false;
    private $options = [];

    private $value = null;

    public function __construct($properties = []){
        if ($properties){
            $this->setProperties($properties);
        }
    }

    private function setProperties($properties){
        foreach($properties as $propertyName => $propertyValue){
            if (property_exists($this, $propertyName)){
                $this->{$propertyName} = $propertyValue;
            }
        }
    }

    public function __get($propertyName){
        if (!property_exists($this, $propertyName)){
            return null;
        }
        return $this->{$propertyName};
    }

    public function isEmpty(){
        return $this->value === null;
    }

    public function isFilled(){
        return !$this->isEmpty();
    }

    public function getValue(){
        return $this->value;
    }

    public function setValue($value){
        $this->value = $value;
    }

    public function clear(){
        $this->value = null;
    }

    public function getOption($optionName, $default=false){
        return array_key_exists($optionName, $this->options) ? $this->options[$optionName] : $default;
    }

    public function renderInput(){
        return Response::render('forms/field', [
            'field' => $this,
        ]);
    }

    public function validate(){

        if ($this->isRequired && !$this->value){
            throw new Exceptions\Validation_Exception("Поле должно быть заполнено");
        }

        if ($this->type == 'email' && $this->value && !filter_var($this->value, FILTER_VALIDATE_EMAIL)){
            throw new Exceptions\Validation_Exception("Некорректный адрес электронной почты");
        }

        if ($this->type == 'password' && !$this->getOption('skipPasswordStrengthCheck')){
            $isLowerUsed = preg_match("/([a-zа-я]+)/", $this->value);
            $isUpperUsed = preg_match("/([A-ZА-Я]+)/", $this->value);
            $isNumbersUsed = preg_match("/([0-9]+)/", $this->value);
            if (!$isLowerUsed || !$isUpperUsed || !$isNumbersUsed){
                throw new Exceptions\Validation_Exception("Слишком простой пароль. Нужны латинские буквы в разных регистрах и цифры.");    
            }
        }

        $isAlphaNumeric = $this->getOption('alphaNumeric');
        if ($isAlphaNumeric && !preg_match("/^([a-z0-9]*)$/i", $this->value)){
            throw new Exceptions\Validation_Exception("Только латинские буквы и цифры");
        }

        $minLength = $this->getOption('minLength');
        if ($minLength && mb_strlen($this->value) < $minLength){
            throw new Exceptions\Validation_Exception("Минимальная длина: {$minLength}");
        }

        $maxLength = $this->getOption('maxLength');
        if ($maxLength && mb_strlen($this->value) > $maxLength){
            throw new Exceptions\Validation_Exception("Максимальная длина: {$maxLength}");
        }

    }

}
