<?php

namespace PhoneBookApp\Core;

class Loader {

    public static function autoload($class){

        $prefix = 'PhoneBookApp\\';
        $base_dir = ROOT_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR;
    
        $prefix_length = strlen($prefix);
        if (strncmp($prefix, $class, $prefix_length) !== 0) {
            return;
        }
    
        $relative_class = substr($class, $prefix_length);
    
        $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
    
        if (file_exists($file)) {
            require $file;
        }
    }

    public static function isFileExists($file){
        return file_exists(self::getAbsoluteFilePath($file));
    }

    public static function include($file){
        return include self::getAbsoluteFilePath($file);
    }

    public static function includeOnce($file){
        return include_once self::getAbsoluteFilePath($file);
    }

    public static function getAbsoluteFilePath($file){
        return path(ROOT_PATH, 'app', str_replace('/', DIRECTORY_SEPARATOR, $file));
    }

}
