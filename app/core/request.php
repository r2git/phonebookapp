<?php

namespace PhoneBookApp\Core;

class Request {

    private static $queryData = [];

    public static function getHost(){
        return self::server('HTTP_HOST');;
    }

    public static function getHostIP(){
        return self::server('SERVER_ADDR');
    }

    public static function get($key, $default = false){
        return empty(self::$queryData[$key]) ? $default : self::$queryData[$key];
    }

    public static function getHas($key){
        return isset(self::$queryData[$key]);
    }

    public static function post($key, $default = false){
        return empty($_POST[$key]) ? $default : $_POST[$key];
    }

    public static function postHas($key){
        return isset($_POST[$key]);
    }

    public static function server($key){
        return filter_input(INPUT_SERVER, $key);
    }

    public static function isAjax(){
        return self::server('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest';
    }

    public static function setQueryData($data){
        self::$queryData = $data;
    }    

    public static function hasFile($key){
        return isset($_FILES[$key]);
    }

}
