<?php

namespace PhoneBookApp\Core;

class Controller {

    public $isAuthRequired = false;

    public function beforeAction(){}
    public function afterAction(){}

    public function isMethodExists($methodName){
        return method_exists($this, $methodName);
    }

    public function isActionExists($actionName){
        return $this->isMethodExists(self::getActionMethodName($actionName));
    }

    public function executeMethod($methodName, $params = []){
        return call_user_func_array([$this, $methodName], $params);
    }

    public function executeAction($actionName, $params = []){

        $actionMethod = self::getActionMethodName($actionName);

        $this->beforeAction();
        $this->executeMethod($actionMethod, $params);
        $this->afterAction();

    }

    private static function getActionMethodName($actionName){
        return 'action' . stringToCamel($actionName);
    }

    public static function get($controllerName){

        $controllerFile = "controllers/{$controllerName}.php";

        if (!Loader::isFileExists($controllerFile)){ return false; }
        
        $controllerClass = "PhoneBookApp\\Controllers\\{$controllerName}";

        $controllerObject = new $controllerClass();

        return $controllerObject;

    }

}
