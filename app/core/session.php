<?php

namespace PhoneBookApp\Core;

use PhoneBookApp\Models\User;

class Session {

    private static $notifications = [];

    private static $user = null;

    public static function init(){
        self::restoreNotifications();
        if (self::isLogged()){            
            self::$user = User::getById($_SESSION['user_id']);
            if (!self::$user){
                self::logout();                
            }
        }
    }

    public static function getUser(){
        return self::$user;
    }

    public static function login($user){
        $_SESSION['user_id'] = $user->id;
    }

    public static function logout(){
        unset($_SESSION['user_id']);
    }    

    public static function isLogged(){
        return isset($_SESSION['user_id']);
    }

    public static function addNotification($notification){
        self::$notifications[] = $notification;
        self::saveNotifications();
    }

    public static function getNotifications(){
        return self::$notifications;
    }

    public static function flushNotifications(){
        unset($_SESSION['notifications']);
        return self::$notifications;
    }

    public static function restoreNotifications(){
        self::$notifications = [];
        if (empty($_SESSION['notifications'])){            
            return;
        }
        foreach($_SESSION['notifications'] as $n){
            self::$notifications[] = Notification::fromArray($n);
        }
    }

    public static function saveNotifications(){
        $saveData = [];
        foreach(self::$notifications as $n){
            $saveData[] = $n->toArray();
        }
    }

}
