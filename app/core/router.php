<?php

namespace PhoneBookApp\Core;

class Router {

    private static $currentURI = '';

    public static function getCurrentURI(){
        return self::$currentURI;
    }

    public static function route(){

        $rawUri = trim(Request::server('REQUEST_URI'), '/');       
        $queryParts = mb_strstr($rawUri, '?') ? explode('?', $rawUri) : [$rawUri];

		if (!empty($queryParts[1])){
            $queryData = [];
            parse_str($queryParts[1], $queryData);
            Request::setQueryData($queryData);
        }

        $uri = $queryParts[0];

        if ($uri && !preg_match('/^([a-zA-Z0-9\-_\/]+)$/i', $uri)){
            Response::error404();
        }

        if (!$uri) {
            self::execute('index');
            return;
        }

        $uriSegments = explode('/', $uri);

        if (empty($uriSegments)){
			$uriSegments = ['index'];
		} else {
			self::$currentURI = implode('/', $uriSegments);
		}

        $controller = $uriSegments[0];
        $action = empty($uriSegments[1]) ? 'index' : $uriSegments[1];
        $params = count($uriSegments) > 2 ? array_slice($uriSegments, 2) : [];

        self::execute($controller, $action, $params);        

    }

    private static function execute($controllerName, $actionName = 'index', $params = []){

        $controller = Controller::get($controllerName);

		if (!$controller) { Response::error404(); }

        if (!$controller->isActionExists($actionName)){ Response::error404(); }

        if ($controller->isAuthRequired && !Session::isLogged()) { Response::error404(); }

        $controller->executeAction($actionName, $params);

        Response::view('page', ['html' => Response::getOutput()]);
        Response::send();

    }    

}
