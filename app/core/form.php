<?php

namespace PhoneBookApp\Core;

class Form {

    private $fields;

    private $id;
    private $errors = null;

    public $endpointURL = '';
    public $submitButtonText = 'Отправить';    
    public $isAjax = false;

    public function __construct($id){
        $this->id = $id;
    }

    public function isSubmitted(){
        return Request::postHas("submit-{$this->id}");
    }

    public function isValid(){
        if ($this->errors !== null){
            return !$this->errors;
        }
        $this->errors = [];
        foreach($this->fields as $field){            
            try {
                $field->validate();
            } catch (Exceptions\Validation_Exception $e) {
                $this->errors[$field->name] = $e->getMessage();
            }
        }        
        return !$this->errors;
    }

    public function hasErrorInField($fieldName){
        return $this->errors && array_key_exists($fieldName, $this->errors);
    }

    public function addFields($fields){
        foreach($fields as $field){
            if (Request::postHas($field->name)){
                $field->setValue(Request::post($field->name));
            }
            $this->fields[$field->name] = $field;            
        }
    }

    public function addField($fieldProperties){
        $field = new Field($fieldProperties);        
        if (Request::postHas($field->name)){
            $field->setValue(Request::post($field->name));
        }
        $this->fields[$field->name] = $field;
        return $field;
    }

    public function render(){
        return Response::render('forms/form', [
            'form' => $this,
            'id' => $this->id,
            'fields' => $this->fields,
            'url' => $this->endpointURL,            
            'submitButtonText' => $this->submitButtonText
        ]);
    }

    public function getResult(){
        $result = [];
        foreach($this->fields as $field){
            $result[$field->name] = Request::post($field->name);            
        }
        return $result;
    }

    public function getErrors(){
        return $this->errors;
    }

    public function clearFields($fieldNames=[]){
        foreach($this->fields as $field){
            if ($fieldNames && !in_array($field->name, $fieldNames)){
                continue;
            }
            $field->clear();
        }
    }

}
