<?php 

namespace PhoneBookApp\Core;

class Model {

    public static $tableName = '';

    private $fields;

    public function __construct(){
        $this->registerField([
            'name' => 'id',
            'type' => 'id',
            'title' => 'Идентификатор',
            'isVirtual' => true
        ]);
    }

    public function registerField($fieldProperties){
        $field = new Field($fieldProperties);        
        $this->fields[$field->name] = $field;
        return $field;
    }

    public static function getFieldsMeta(){
        $modelClass = get_called_class();
        $object = new $modelClass();
        return $object->getFields();
    }

    public static function getForm(){
        $fieldsMeta = static::getFieldsMeta();
        $form = new Form(static::$tableName . '-form');
        $form->addFields($fieldsMeta);
        return $form;
    }

    public static function getById($id){

        $objects = Database::select(static::$tableName, ['id' => $id]);

        if (!$objects){
            return null;
        }

        return self::createFromArray($objects[0]);

    }

    public static function getByField($fieldName, $value){
        return self::getAll([$fieldName => $value]);
    }

    public static function getAll($conditions = []){

        $objects = Database::select(static::$tableName, $conditions);

        if (!$objects){
            return null;
        }

        $result = [];
        foreach($objects as $object){
            $result[] = self::createFromArray($object);
        }

        return $result;

    }

    public static function createFromArray($values){
        $modelClass = get_called_class();
        $object = new $modelClass();
        $modelFields = $object->getFields();        
        foreach($values as $name => $value){
            if (!array_key_exists($name, $modelFields)){
                continue;
            }
            $object->{$name} = $value;
        }
        return $object;
    }

    public function create(){
        $this->id = Database::insert(static::$tableName, $this->getFieldValues());
    }

    public function update(){
        Database::update(static::$tableName, $this->getFieldValues(), ['id' => $this->id]);
    }

    public function getFieldValues(){
        return array_map(function($field) {
            return $field->getValue();
        }, $this->fields);
    }

    public function getFields(){
        return $this->fields;
    }

    public function __get($fieldName){
        if (array_key_exists($fieldName, $this->fields)){
            return $this->fields[$fieldName]->getValue();
        }
        return null;
    }

    public function __set($fieldName, $value){
        if (array_key_exists($fieldName, $this->fields)){
            $this->fields[$fieldName]->setValue($value);
        }
    }

    public function delete(){
        Database::delete(static::$tableName, ['id' => $this->id]);
    }

    public function save(){
        if ($this->isNew()){
            $this->create();
        } else {
            $this->update();
        }
    }

    public function uploadFiles() {
        foreach($this->fields as $field){

            if ($field->type !== 'file') { 
                continue; 
            }

            if (!Request::hasFile($field->name)){ 
                $this->{$field->name} = null;
                continue; 
            }

            $destination = join('/', [
                date('Y'), date('m'), date('d'), basename($_FILES[$field->name]['name'])
            ]);

            $this->{$field->name} = $destination;

            $absPath = path(UPLOAD_PATH, $destination);            
            @mkdir(dirname($absPath), 0777, true);

            move_uploaded_file($_FILES[$field->name]['tmp_name'], $absPath);

        }
    }

    public function isNew() {
        return $this->id == null;
    }

}
