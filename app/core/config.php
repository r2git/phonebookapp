<?php

namespace PhoneBookApp\Core;

class Config {

    private static $data = [];

    public static function get($key, $default=false){
        if (!self::$data) { self::load(); }
        return isset(self::$data[$key]) ? self::$data[$key] : $default;
    }

    public static function is($key){
        return !empty(self::$data[$key]);
    }

    private static function load(){
        self::$data = Loader::includeOnce('config.php');
    }

    public static function isProd(){
        return !self::isDev();
    }

    public static function isDev(){
        return Request::getHostIP() == '127.0.0.1';
    }

}
