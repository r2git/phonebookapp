<?php 
    $notifications = \PhoneBookApp\Core\Session::flushNotifications();
    if (!$notifications) { return; }
?>
<div class="notifications">
    <?php foreach($notifications as $n) { ?>
        <div class="item type-<?=$n->type;?>">
            <?=$n->message;?>
        </div>
    <?php } ?>
</div>
