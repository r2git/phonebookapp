<?php 
    use \PhoneBookApp\Core\Response; 
    Response::setTitle('Регистрация - Телефонная книга');
?>

<h1 class="page-title">
    Телефонная книга
</h1>

<div class="app-menu">
    <a href="<?=url('/');?>">Войти</a> | Регистрация
</div>

<div class="login-form">
    <?=Response::render('notifications');?>
    <?=$signupForm->render();?>
</div>
