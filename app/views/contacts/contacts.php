<?php 
    use PhoneBookApp\Core\Response; 
    Response::setTitle('Контакты - Телефонная книга')
?>

<h1 class="page-title">
    Телефонная книга
</h1>

<div class="app-menu">
    <a href="#" id="add-contact-button">Добавить контакт</a> | 
    <a href="<?=url('logout');?>">Выйти</a>
</div>

<div class="add-contact-form">
    <div class="close-button">
        <i class="fa fa-close"></i>
    </div>      
    <h3>Добавить контакт</h3>    
    <?=$contactForm->render();?>
    <div class="loader">
        <i class="fa fa-spin fa-spinner"></i>
    </div>
</div>

<div class="contact-list">
    <?php if ($contacts){ ?>
        <?php foreach($contacts as $contact) { ?>
            <?=$contact->render();?>
        <?php } ?>
    <?php } ?>

    <?php if (!$contacts){ ?>
        <p class="empty">Ваш список контактов сейчас пуст</p>
    <?php } ?>
</div>
