<?php 
    use PhoneBookApp\Core\Response; 
    Response::setTitle($contact->getFullName())
?>

<h1 class="page-title">
    Телефонная книга
</h1>

<div class="app-menu">
    &larr; <a href="<?=url('contacts');?>">Назад к списку</a> | 
    <a href="<?=url("contacts/delete/{$contact->id}");?>">Удалить контакт</a> | 
    <a href="<?=url('logout');?>">Выйти</a>
</div>

<h2><?=$contact->getFullName();?></h2>

<div class="contact-details">
    <div class="row">
        <div class="title">Телефон</div>
        <div class="value"><?=$contact->phone;?></div>
    </div>
    <div class="row">
        <div class="title">Электронная почта</div>
        <div class="value"><?=($contact->email ? $contact->email : 'Не указана');?></div>
    </div>
</div>

<?php if ($contact->image) { ?>
    <div class="contact-avatar" style="background-image: url(/uploads/<?=$contact->image;?>)">
    </div>
<?php } ?>
