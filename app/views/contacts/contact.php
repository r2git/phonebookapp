<div class="contact">
    <div class="name">
        <a href="<?=url("contacts/view/{$contact->id}");?>"><?=$contact->getFullName();?></a>
    </div>
    <div class="phone"><?=$contact->phone;?></div>
    <div class="actions">
        <a href="<?=url("contacts/delete/{$contact->id}");?>" title="Удалить контакт" class="delete">
            <i class="fa fa-close"></i>
        </a>
    </div>
</div>
