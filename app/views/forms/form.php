<div class="app-form">
    <form action="<?=$url;?>" method="post" enctype="multipart/form-data">
        <div class="fields">
            <?php foreach($fields as $field){ ?>
                <?php if ($field->isVirtual) { continue; } ?>
                <div class="field<?php if ($form->hasErrorInField($field->name)) {?> error<?php } ?>">
                    <label><?=$field->title;?></label>
                    <div class="input">
                        <?=$field->renderInput();?>
                    </div>
                    <?php if ($form->hasErrorInField($field->name)) {?>
                        <div class="error"><?=$form->getErrors()[$field->name];?></div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <div class="buttons">
            <input type="submit" class="submit-button" name="submit-<?=$id;?>" value="<?=$submitButtonText;?>">
        </div>
    </form>
</div>
