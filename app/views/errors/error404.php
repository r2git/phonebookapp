<html>
<head>
    <meta charset="UTF-8" />
    <title>Страница не найдена</title>
    <link rel="stylesheet" href="/static/css/errors.css">    
</head>
<body>
	<section class="error-page">
		<h1>Страница не найдена</h1>
        <a href="<?=url('/')?>">Вернуться на главную</a>
	</section>
</body>
</html>
