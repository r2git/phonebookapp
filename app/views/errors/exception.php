<html>
<head>
    <meta charset="UTF-8" />
    <title>Произошла ошибка</title>
    <link rel="stylesheet" href="/static/css/errors.css">    
</head>
<body>
	<section class="error-page">
		<h1>Произошла ошибка</h1>
        <div class="error-text"><?=$errorText;?></div>        
	</section>
</body>
</html>
