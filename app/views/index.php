<?php 
    use PhoneBookApp\Core\Response; 
    Response::setTitle('Телефонная книга')
?>

<h1 class="page-title">
    Телефонная книга
</h1>

<div class="app-menu">
    Войти | <a href="<?=url('signup');?>">Регистрация</a>
</div>

<div class="login-form">
    <?=Response::render('notifications');?>
    <?=$loginForm->render();?>
</div>
