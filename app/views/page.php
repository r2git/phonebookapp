<?php 
    use PhoneBookApp\Core\Response;
    use PhoneBookApp\Core\Session;
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title><?=Response::getTitle();?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap">
    <link rel="stylesheet" href="/static/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/css/styles.css">
</head>
<body>

    <div class="app-page">
        <?=$html;?>
    </div>

    <?php if (Session::isLogged()) { ?>
        <script src="/static/js/jquery-3.5.1.min.js"></script>
        <script src="/static/js/app.js"></script>
    <?php } ?>

</body>

</html>
