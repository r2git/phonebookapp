<?php return [

    'root_url' => '/',  

    'db_type' => 'mysql',
    'db_host' => 'localhost',
    'db_name' => 'phonebookapp',
    'db_user' => 'root',
    'db_pass' => '',
    'db_charset' => 'utf8mb4'

];
