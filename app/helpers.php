<?php

function path(){
    return trim(join(DIRECTORY_SEPARATOR, func_get_args()), DIRECTORY_SEPARATOR);
}

function url($controllerName, $actionName='index', $queryData=[]){

    $rootUrl = \PhoneBookApp\Core\Config::get('root_url');

    if (!$controllerName || $controllerName == '/'){
        return $rootUrl;
    }

    $uri = $controllerName;

    if ($actionName != 'index'){
        $uri .= '/' . $actionName;
    }

    if ($queryData){
        $uri .= '?' . http_build_query($queryData);
    }

    return $rootUrl . trim($uri, '/');

}

function stringToCamel($string, $delimiter = '-'){

    $result = '';
    $words = explode($delimiter, mb_strtolower($string));

    foreach($words as $word){
        $result .= ucfirst($word);
    }

    return $result;

}

function dump($var){
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
    die();
}