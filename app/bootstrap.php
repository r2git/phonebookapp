<?php

    session_start();
    
    define('ROOT_PATH', dirname(__DIR__));
    define('UPLOAD_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR);

    mb_internal_encoding('UTF-8');

    require_once ROOT_PATH . '/app/helpers.php';
    require_once ROOT_PATH . '/app/core/loader.php';
    spl_autoload_register(['\PhoneBookApp\Core\Loader', 'autoload']);

    \PhoneBookApp\Core\Database::init();
    \PhoneBookApp\Core\Session::init();    
