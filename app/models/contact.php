<?php

namespace PhoneBookApp\Models;

use PhoneBookApp\Core\Response;
use PhoneBookApp\Core\Model;

class Contact extends Model {

    public static $tableName = 'contacts';

    public function __construct(){
        
        parent::__construct();

        $this->registerField([
            'name' => 'user_id',
            'type' => 'number',
            'title' => 'Владелец контакта',
            'isVirtual' => true
        ]);

        $this->registerField([
            'name' => 'name',
            'type' => 'string',
            'title' => 'Имя',
            'isRequired' => true,
            'options' => [
                'maxLength' => 255
            ]
        ]);

        $this->registerField([
            'name' => 'surname',
            'type' => 'string',
            'title' => 'Фамилия',
            'options' => [
                'maxLength' => 255
            ]
        ]);

        $this->registerField([
            'name' => 'phone',
            'type' => 'string',
            'title' => 'Номер телефона',
            'isRequired' => true,
            'options' => [
                'maxLength' => 32
            ]
        ]);

        $this->registerField([
            'name' => 'email',
            'type' => 'email',
            'title' => 'Электронная почта',
            'options' => [
                'maxLength' => 255
            ]
        ]);

        $this->registerField([
            'name' => 'image',
            'type' => 'file',
            'title' => 'Фотография',
            'options' => [                
            ]
        ]);        

    }

    public function getFullName(){
        return trim(join(' ', [$this->name, $this->surname]));
    }

    public function render(){
        return Response::render('contacts/contact', [
            'contact' => $this
        ]);
    }

}

