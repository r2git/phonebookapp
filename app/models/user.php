<?php

namespace PhoneBookApp\Models;

use PhoneBookApp\Core\Model;

class User extends Model {

    public static $tableName = 'users';

    public function __construct(){
        
        parent::__construct();

        $this->registerField([
            'name' => 'login',
            'type' => 'string',
            'title' => 'Логин',
            'isRequired' => true,
            'options' => [
                'minLength' => 3,
                'maxLength' => 16,
                'alphaNumeric' => true
            ],
        ]);

        $this->registerField([
            'name' => 'email',
            'type' => 'email',
            'title' => 'Электронная почта',
            'isRequired' => true,
            'options' => [
                'maxLength' => 255
            ]
        ]);

        $this->registerField([
            'name' => 'password',
            'type' => 'password',
            'title' => 'Пароль',
            'isRequired' => true,
            'options' => [
                'minLength' => 6,
                'maxLength' => 32
            ]
        ]);                

    }

    public static function getPasswordHash($password){
        return md5(md5($password));
    }

    public function save(){

        if (self::getByField('email', $this->email)){
            throw new \PhoneBookApp\Core\Exceptions\Validation_Exception("Почтовый ящик уже зарегистрирован");
        }

        $this->password = self::getPasswordHash($this->password);

        parent::create();

    }

}
